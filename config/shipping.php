<?php

// config for Bittacora/Shipping
return [
    // % de impuesto aplicado por defecto al cálculo de los gastos de envío al crear zonas de envío.
    // Se aplica de manera implícita si no se indica otro porcentaje en los métodos correspondientes
    'shipping_vat' => 21
];

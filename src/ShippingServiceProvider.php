<?php

namespace Bittacora\Shipping;

use Bittacora\Shipping\Http\Livewire\ShippingDatatable;
use Livewire\Livewire;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use Bittacora\Shipping\Commands\ShippingCommand;

class ShippingServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('shipping')
            ->hasConfigFile()
            ->hasViews()
            ->hasMigration('create_shipping_table')
            ->hasCommand(ShippingCommand::class);
    }

    public function register(): void{
        parent::register();
        $this->loadJsonTranslationsFrom(__DIR__ . '/../../../rappasoft/laravel-livewire-tables/resources/lang/');
        $this->app->bind('shipping', function($app){
            return new Shipping();
        });
    }

    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadMigrationsFrom(__DIR__ .'/../database/migrations');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'shipping');
        $this->loadTranslationsFrom(__DIR__ .'/../resources/lang', 'shipping');
        if ($this->app->runningInConsole()) {
            $this->commands([
                ShippingCommand::class
            ]);
        }

        $this->publishes([
            __DIR__ . "/../config/shipping.php" => config_path('shipping.php')
        ]);


        Livewire::component('shipping::shipping-datatable', ShippingDatatable::class);
    }
}

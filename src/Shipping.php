<?php

namespace Bittacora\Shipping;

use Bittacora\Shipping\Infrastructure\ShippingAreaRepository;
use Bittacora\Shipping\Infrastructure\ShippingPriceRangeRepository;
use Bittacora\Shipping\Infrastructure\ShippingRepository;
use Bittacora\Shipping\Infrastructure\ShippingWeightRepository;
use Bittacora\Shipping\Models\ShippingModel;
use Illuminate\Database\Eloquent\Collection;

class Shipping
{
    private $shippingRepository;
    private $shippingAreaRepository;
    private $shippingWeightRepository;
    private $shippingPriceRangeRepository;

    public function __construct()
    {
        $this->shippingRepository = new ShippingRepository();
        $this->shippingAreaRepository = new ShippingAreaRepository();
        $this->shippingWeightRepository = new ShippingWeightRepository();
        $this->shippingPriceRangeRepository = new ShippingPriceRangeRepository();
    }

    /**
     * Obtenemos gastos de envío por rango de precios asociados a una zona de envío
     * @param ShippingModel $shipping Zona de envío seleccionada
     * @param float $subtotal Subtotal del carrito
     * @param int|null $stateSelectedForShipping Provincia/estado seleccionado
     * @return float
     */
    public function getShippingByPriceRange(ShippingModel $shipping, float $subtotal, null|int $stateSelectedForShipping){
        return $this->shippingPriceRangeRepository->getShipping($shipping, $subtotal, $stateSelectedForShipping);
    }

    /**
     * Devuelve los gastos de envío por peso asociados a una zona de envío
     * @param ShippingModel $shipping Zona de envío seleccionada
     * @param float $weight Peso total del carrito
     * @return float Provincia/estado seleccionado
     */
    public function getShippingByWeight(ShippingModel $shipping, float $weight, null|int $stateSelectedForShipping){
        return $this->shippingWeightRepository->getShipping($shipping, $weight, $stateSelectedForShipping);
    }

    /**
     * Devuelve la zona de envío a la que pertenece un estado/provincia
     * @param int $stateId ID del estado/provincia
     * @return ShippingModel
     */
    public function getShippingByState(int $stateId){
        return $this->shippingAreaRepository->getShippingByState($stateId);
    }

    /**
     * Devuelve todos los países que están asociados a alguna zona de envío
     * @return array
     */
    public function getCountriesAvailableForShipping(){
        return $this->shippingRepository->getCountriesAvailableForShipping();
    }

    /**
     * Devuelve todos los estados que estén asociados a alguna zona de envío
     * @param Collection|ShippingModel $model
     * @return mixed[]
     */
    public function getSelectedAreasForShipping(Collection|ShippingModel $model){
        return $this->shippingAreaRepository->getSelectedAreasForShipping($model);
    }

    /**
     * Obtenemos zonas de envío asociadas a un país
     * @param int $countryId
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getShippingRelatedToCountry(int $countryId){
        return $this->shippingRepository->getShippingRelatedToCountry($countryId);
    }
}

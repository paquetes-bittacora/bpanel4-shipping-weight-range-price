<?php

namespace Bittacora\Shipping\Commands;

use Bittacora\AdminMenu\AdminMenuFacade;
use Bittacora\Tabs\Tabs;
use Illuminate\Console\Command;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class ShippingCommand extends Command
{
    public $signature = 'shipping:install';

    public $description = 'Añade permisos y elementos del menú del módulo de zonas de envío';

    public function handle()
    {
        $this->comment('Registrando elementos del menú...');
        $group = AdminMenuFacade::createGroup('ecommerce', 'Comercio', 'fa fa-shopping-cart');
        $module = AdminMenuFacade::createModule($group->key, 'shipping', 'Zonas de envío', 'fas fa-shipping-fast');
        AdminMenuFacade::createAction($module->key, 'Listar', 'index', 'fa fa-bars');
        AdminMenuFacade::createAction($module->key, 'Añadir', 'create', 'fa fa-plus');

        $this->comment('Hecho');

        $this->comment('Dando permisos al administrador...');
        $permissions = ['index', 'create', 'edit', 'destroy', 'store', 'update'];


        $adminRole = Role::findOrCreate('admin');
        foreach ($permissions as $permission) {
            $permission = Permission::firstOrCreate(['name' => 'shipping.' . $permission]);
            $adminRole->givePermissionTo($permission);
        }

        Tabs::createItem('shipping.index', 'shipping.index', 'shipping.index', 'Zonas de envío', 'fas fa-shipping-fast');
        Tabs::createItem('shipping.index', 'shipping.create', 'shipping.create', 'Añadir', 'fa fa-plus');
        Tabs::createItem('shipping.create', 'shipping.index', 'shipping.index', 'Zonas de envío', 'fas fa-shipping-fast');
        Tabs::createItem('shipping.create', 'shipping.create', 'shipping.create', 'Añadir', 'fa fa-plus');
        Tabs::createItem('shipping.edit', 'shipping.index', 'shipping.index', 'Zonas de envío', 'fas fa-shipping-fast');
        Tabs::createItem('shipping.edit', 'shipping.create', 'shipping.create', 'Añadir', 'fa fa-plus');


        $this->comment('Hecho');
    }
}

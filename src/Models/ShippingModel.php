<?php

namespace Bittacora\Shipping\Models;

use Bittacora\LivewireCountryStateSelector\Models\Country;
use Bittacora\Shipping\Database\Factories\ShippingFactory;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Wildside\Userstamps\Userstamps;

class ShippingModel extends Model
{
    use Userstamps, HasFactory;

    protected $table = 'shipping';
    protected $dateFormat = 'Y-m-d H:i:s';

    protected $fillable = [
        'name', 'country_id', 'active', 'free_shipping', 'by_weight'
    ];

    protected $casts = ['date' => 'datetime'];

    protected static function newFactory()
    {
        return ShippingFactory::new();
    }

    public function country(){
        return $this->hasOne(Country::class, 'country_id');
    }
    public function getCountryAttribute(){
        $country = Country::where('id', $this->country_id)->first();
        return $country;
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}

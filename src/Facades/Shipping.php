<?php

namespace Bittacora\Shipping\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Bittacora\Shipping\Shipping
 */
class Shipping extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \Bittacora\Shipping\Shipping::class;
    }
}

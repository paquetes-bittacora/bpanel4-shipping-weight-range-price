<?php

namespace Bittacora\Shipping\Http\Livewire;

use Bittacora\Shipping\Models\ShippingModel;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use App\Models\User;

class ShippingDatatable extends DataTableComponent
{
    protected $model = ShippingModel::class;

    public function configure(): void
    {
        $this->setPrimaryKey('id')
            ->setTheadAttributes([
                'style' => 'text-align:center'
            ]);
    }

    public function columns(): array
    {
        return [
            Column::make("Nombre", "name"),
            Column::make("Activo", "active")->view('shipping::datatable.active-column'),
            Column::make("Envío gratuito", "free_shipping")->view('shipping::datatable.free-shipping-column'),
            Column::make("Envío por peso", "by_weight")->view('shipping::datatable.by-weight-column'),
            Column::make("Acciones", "id")->view('shipping::datatable.actions-column')
        ];
    }
}

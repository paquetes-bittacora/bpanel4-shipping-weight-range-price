<?php

namespace Bittacora\Shipping\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\LivewireCountryStateSelector\Models\Country;
use Bittacora\LivewireCountryStateSelector\Models\State;
use Bittacora\Shipping\Http\Requests\StoreShippingRequest;
use Bittacora\Shipping\Http\Requests\UpdateShippingRequest;
use Bittacora\Shipping\Infrastructure\ShippingAreaRepository;
use Bittacora\Shipping\Infrastructure\ShippingPriceRangeRepository;
use Bittacora\Shipping\Infrastructure\ShippingRepository;
use Bittacora\Shipping\Infrastructure\ShippingWeightRepository;
use Bittacora\Shipping\Models\ShippingModel;
use Illuminate\Http\Request;

class ShippingController extends Controller
{
    private $shippingRepository;
    private $shippingAreaRepository;
    private $shippingWeightRepository;
    private $shippingPriceRangeRepository;

    public function __construct()
    {
        $this->shippingRepository = new ShippingRepository();
        $this->shippingAreaRepository = new ShippingAreaRepository();
        $this->shippingWeightRepository = new ShippingWeightRepository();
        $this->shippingPriceRangeRepository = new ShippingPriceRangeRepository();
    }

    public function index(){
        $this->authorize('shipping.index');
        return view('shipping::index');
    }

    public function create(){
        $this->authorize('shipping.create');
        $countries = Country::orderBy('name', 'ASC')->get()->pluck('name', 'id')->toArray();
        return view('shipping::create', compact('countries'));
    }

    public function store(StoreShippingRequest $request){
        $this->authorize('shipping.store');
        $model = new ShippingModel();
        $model->fill($request->all())->save();
        return redirect()->route('shipping.edit', ['model' => $model])->with(['alert-success' => 'La zona de envío ha sido creada correctamente']);
    }

    public function edit(ShippingModel $model){
        $this->authorize('shipping.edit');
        $states = State::where('country_id', $model->country_id)->orderBy('name', 'ASC')->get(['id', 'name']);
        $statesForSelect = [];
        foreach($states as $key => $value){
            $statesForSelect[$value->id] = $value->name;
        }
        return view('shipping::edit', ['model' => $model, 'states' => $statesForSelect,
            'selectedAreas' => $this->shippingAreaRepository->getSelectedAreasForShipping($model),
            'selectedWeight' => $this->shippingWeightRepository->getSelectedWeightForShipping($model),
            'selectedPriceRange' => $this->shippingPriceRangeRepository->getSelectedPriceRangeForShipping($model)
        ]);
    }

    public function update(UpdateShippingRequest $request, ShippingModel $model){
        $this->authorize('shipping.update');
        $model->fill($request->all())->save();
        $this->shippingAreaRepository->associateAreasToShipping($model, $request->input('state_id'));
        $this->shippingWeightRepository->associateWeightToShipping($model, $request->input('min_weight'),
            $request->input('max_weight'), $request->input('price_including_vat'));

        $this->shippingPriceRangeRepository->associatePriceRangeToShipping($model, $request->input('min_price'),
            $request->input('max_price'), $request->input('range_price_including_vat'));
        return redirect()->route('shipping.edit', ['model' => $model])->with(['alert-success' => 'La zona de envío ha sido modificada correctamente']);
    }

    /**
     * @param ShippingModel $model
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(ShippingModel $model){
        $this->authorize('shipping.destroy');
        if($model->delete()){
            return redirect()->route('shipping.index')->with(['alert-success' => 'La zona de envío ha sido eliminada correctamente']);
        }else{
            return redirect()->route('shipping.index')->with(['alert-danger' => 'La zona de envío no pudo ser eliminada']);
        }
    }
}

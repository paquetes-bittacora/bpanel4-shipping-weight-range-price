<?php

namespace Bittacora\Shipping\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreShippingRequest extends FormRequest
{
    public function prepareForValidation()
    {
        if($this->has('active')){
            $this->request->add(['active' => 1]);
        }else{
            $this->request->add(['active' => 0]);
        }

        if($this->has('free_shipping')){
            $this->request->add(['free_shipping' => 1]);
        }else{
            $this->request->add(['free_shipping' => 0]);
        }

        if($this->has('by_weight')){
            $this->request->add(['by_weight' => 1]);
        }else{
            $this->request->add(['by_weight' => 0]);
        }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('shipping.store');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'country_id' => 'required',
            'active' => 'required',
            'free_shipping' => 'required',
            'by_weight' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Debe introducir un nombre para la zona de envío',
            'country_id.required' => 'Debe seleccionar un país',
            'active.required' => 'Debe marcar el parámetro activo'
        ];
    }
}

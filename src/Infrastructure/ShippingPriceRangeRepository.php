<?php

namespace Bittacora\Shipping\Infrastructure;

use Bittacora\Shipping\Models\ShippingModel;
use Bittacora\Vat\Facades\Vat;
use Illuminate\Support\Facades\DB;

class ShippingPriceRangeRepository
{
    private $shippingVat = null;

    public function __construct(null|int|float $shippingVat = null)
    {
        if(!is_null($shippingVat)){
            $this->shippingVat = $shippingVat;
        }else{
            $this->shippingVat = config('shipping')['shipping_vat'];
        }
    }

    /**
     * Devuelve los gastos de envío por rango de precios asociados a una zona de envío
     * @param ShippingModel $shipping
     * @param float $subtotal
     * @return float
     */
    public function getShipping(ShippingModel $shipping, float $subtotal, null|int $stateSelectedForShipping = null): float{
        $shippingPriceRange = DB::table('shipping_price_range')->where(function($query) use ($shipping, $subtotal){
            $query->where('shipping_id', $shipping->id);
            $query->where('min_price', '<=', $subtotal);
            $query->where('max_price', '>=', $subtotal);
        })->first();

        if(is_null($shippingPriceRange)){
            return 0.00;
        }else{
            if(!is_null($stateSelectedForShipping)){
                $vatModel = Vat::getShippingVatRate();
                if(is_null($vatModel)){
                    $vatModelRegion = $vatModel->regions()->where('state_id', $stateSelectedForShipping)->first();
                    if(!is_null($vatModelRegion)){
                        return $shippingPriceRange->price_excluding_vat*(1+($vatModelRegion->vat/100));
                    }else{
                        return $shippingPriceRange->price_including_vat;
                    }
                }else{
                    return $shippingPriceRange->price_excluding_vat*(1+$this->shippingVat/100);
                }

            }else{
                return $shippingPriceRange->price_including_vat;
            }
        }
    }
    /**
     * Recuperamos los tramos de peso asociados a una zona de envío
     * @param ShippingModel $model
     * @return \Illuminate\Support\Collection
     */
    public function getSelectedPriceRangeForShipping(ShippingModel $model){
        $selectedPriceRange = DB::table('shipping_price_range')->where('shipping_id', $model->id)->get();
        return $selectedPriceRange;
    }


    /**
     * Asociamos tramos de precio a una zona de envío
     * @param ShippingModel $model
     * @param array $minPriceArr
     * @param array $maxPriceArr
     * @param array $priceIncludingVatArr
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function associatePriceRangeToShipping(ShippingModel $model, ?array $minPriceArr, ?array $maxPriceArr, ?array $priceIncludingVatArr){
        $this->removePriceRangeFromShipping($model);
        if(!is_null($minPriceArr) and !is_null($maxPriceArr) and !is_null($priceIncludingVatArr)){
            foreach($minPriceArr as $key => $value){
                if($value !== ""){
                    $connection = DB::table('shipping_price_range');
                    if($maxPriceArr[$key] !== "" and $priceIncludingVatArr[$key] !== ""){
                        $shippingVatRate = Vat::getShippingVatRate();
                        if(is_null($shippingVatRate)){
                            $priceIncludingVat = $priceIncludingVatArr[$key];
                            $priceExcludingVat = $priceIncludingVat / (1+($this->shippingVat/100));
                        }else{
                            $priceIncludingVat = $priceIncludingVatArr[$key];
                            $priceExcludingVat = $priceIncludingVat / (1+($shippingVatRate->vat/100));
                        }
                        $connection->insert([
                            'shipping_id' => $model->id,
                            'min_price' => $value,
                            'max_price' => $maxPriceArr[$key],
                            'price_including_vat' => $priceIncludingVat,
                            'price_excluding_vat' => $priceExcludingVat,
                            'price_vat' => $priceIncludingVat - $priceExcludingVat
                        ]);
                        $connection->newQuery();
                    }else{
                        $this->removePriceRangeFromShipping($model);
                        return redirect()->route('shipping.edit', ['model' => $model])
                            ->with(['alert-danger' => 'Hubo un error en los datos de los tramos de precio y no pudieron ser almacenados']);
                    }
                }
            }
        }
    }

    /**
     * Elimina los tramos de precios existentes asociados a una zona de envío
     * @param ShippingModel $model
     */
    private function removePriceRangeFromShipping(ShippingModel $model){
        $connection = DB::table('shipping_price_range');
        $connection->where('shipping_id', $model->id)->delete();
    }
}

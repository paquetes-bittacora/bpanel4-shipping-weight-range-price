<?php

namespace Bittacora\Shipping\Infrastructure;

use Bittacora\Shipping\Models\ShippingModel;
use Bittacora\Vat\Facades\Vat;
use Illuminate\Support\Facades\DB;

class ShippingWeightRepository
{
    private $shippingVat = null;

    public function __construct(null|int|float $shippingVat = null)
    {
        if(!is_null($shippingVat)){
            $this->shippingVat = $shippingVat;
        }else{
            $this->shippingVat = config('shipping')['shipping_vat'];
        }
    }

    /**
     * Devuelve los gastos de envío por peso asociados a una zona de envío
     * @param ShippingModel $shipping Zona de envío
     * @param float $weight Peso del pedido
     * @param null|int $stateSelectedForShipping Provincia/estado seleccionada para el envío. Este parámetro es el que
     * se evalúa para seleccionar que IVA se le tiene que aplicar al envío, evaluando previamente las regiones
     * asociadas al tipo de IVA
     * @return float Gastos de envío con el IVA aplicable incluido
     */
    public function getShipping(ShippingModel $shipping, float $weight, null|int $stateSelectedForShipping = null): float{
        $shippingWeight = DB::table('shipping_weight')->where(function($query) use ($shipping, $weight){
            $query->where('shipping_id', $shipping->id);
            $query->where('min_weight', '<=', $weight);
            $query->where('max_weight', '>=', $weight);
        })->first();

        if(is_null($shippingWeight)){
            return 0.00;
        }else{
            if(!is_null($stateSelectedForShipping)){
                $vatModel = Vat::getShippingVatRate();
                if(is_null($vatModel)){
                    $vatModelRegion = $vatModel->regions()->where('state_id', $stateSelectedForShipping)->first();
                    if(!is_null($vatModelRegion)){
                        return $shippingWeight->price_excluding_vat*(1+($vatModelRegion->vat/100));
                    }else{
                        return $shippingWeight->price_including_vat;
                    }
                }else{
                    return $shippingWeight->price_excluding_vat*(1+$this->shippingVat/100);
                }

            }else{
                return $shippingWeight->price_including_vat;
            }
        }
    }

    /**
     * Recuperamos los tramos de peso asociados a una zona de envío
     * @param ShippingModel $model
     * @return \Illuminate\Support\Collection
     */
    public function getSelectedWeightForShipping(ShippingModel $model){
        $selectedWeight = DB::table('shipping_weight')->where('shipping_id', $model->id)->get();
        return $selectedWeight;
    }

    /**
     * Asociamos tramos de peso a una zona de envío
     * @param ShippingModel $model
     * @param array $minWeightArr
     * @param array $maxWeightArr
     * @param array $priceIncludingVatArr
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function associateWeightToShipping(ShippingModel $model, ?array $minWeightArr, ?array $maxWeightArr, ?array $priceIncludingVatArr){
        $this->removeWeightFromShipping($model);
        if(!is_null($minWeightArr) and !is_null($maxWeightArr) and !is_null($priceIncludingVatArr)){
            foreach($minWeightArr as $key => $value){
                if($value !== ""){
                    $connection = DB::table('shipping_weight');
                    if($maxWeightArr[$key] !== "" and $priceIncludingVatArr[$key] !== ""){
                        $shippingVatRate = Vat::getShippingVatRate();
                        if(is_null($shippingVatRate)){
                            $priceIncludingVat = $priceIncludingVatArr[$key];
                            $priceExcludingVat = $priceIncludingVat / (1+($this->shippingVat/100));
                        }else{
                            $priceIncludingVat = $priceIncludingVatArr[$key];
                            $priceExcludingVat = $priceIncludingVat / (1+($shippingVatRate->vat/100));
                        }

                        $connection->insert([
                            'shipping_id' => $model->id,
                            'min_weight' => $value,
                            'max_weight' => $maxWeightArr[$key],
                            'price_including_vat' => $priceIncludingVat,
                            'price_excluding_vat' => $priceExcludingVat,
                            'price_vat' => $priceIncludingVat - $priceExcludingVat
                        ]);
                        $connection->newQuery();
                    }else{
                        $this->removeWeightFromShipping($model);
                        return redirect()->route('shipping.edit', ['model' => $model])
                            ->with(['alert-danger' => 'Hubo un error en los datos de los tramos de peso y no pudieron ser almacenados']);
                    }
                }
            }
        }
    }

    /**
     * Elimina los tramos de pesos existentes asociados a una zona de envío
     * @param ShippingModel $model
     */
    private function removeWeightFromShipping(ShippingModel $model){
        $connection = DB::table('shipping_weight');
        $connection->where('shipping_id', $model->id)->delete();
    }

}

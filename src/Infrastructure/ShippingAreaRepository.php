<?php

namespace Bittacora\Shipping\Infrastructure;

use Bittacora\Shipping\Models\ShippingModel;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class ShippingAreaRepository
{

    /**
     * Devuelve la zona de envío a la que pertenece un estado/provincia
     * @param int $stateId
     * @return ShippingModel
     */
    public function getShippingByState(int $stateId): ShippingModel{
        $shippingByState = DB::table('shipping_area')->where('state_id', $stateId)->first();
        $shipping = ShippingModel::where('id', $shippingByState->shipping_id)->first();
        return $shipping;
    }

    /**
     * Devuelve los IDs de los estados o provincias asociadas a una zona de envío
     * @param ShippingModel $model
     * @return mixed[]
     */
    public function getSelectedAreasForShipping(Collection|ShippingModel $model)
    {
        $selectedAreas = [];

        if($model instanceof Collection){
            foreach($model as $item){
                $itemStateIds = DB::table('shipping_area')->where('shipping_id', $item->id)->pluck('state_id')->toArray();
                $selectedAreas = array_merge($selectedAreas, $itemStateIds);
            }
        }else{
            $selectedAreas = DB::table('shipping_area')->where('shipping_id', $model->id)->pluck('state_id')->toArray();
        }

        return $selectedAreas;
    }

    /**
     * Asocia provincias o estados a una zona de envío
     * @param ShippingModel $model
     * @param array $stateIds
     */
    public function associateAreasToShipping(ShippingModel $model, ?array $stateIds){
        $this->removeAreasFromShipping($model);
        if(!is_null($stateIds)){
            foreach($stateIds as $stateId){
                DB::table('shipping_area')->insert([
                    'shipping_id' => $model->id,
                    'state_id' => $stateId
                ]);
            }
        }
    }

    /**
     * Elimina las provicinas o estados asociados a una zona de envío
     * @param ShippingModel $model
     */
    private function removeAreasFromShipping(ShippingModel $model){
        $connection = DB::table('shipping_area');
        $connection->where('shipping_id', $model->id)->delete();
    }
}

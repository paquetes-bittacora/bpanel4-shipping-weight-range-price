<?php

namespace Bittacora\Shipping\Infrastructure;

use Bittacora\Shipping\Models\ShippingModel;
use Illuminate\Database\Eloquent\Collection;

class ShippingRepository
{

    /**
     * Devuelve los IDs de los países que están presentes en alguna zona de envío
     * @return array
     */
    public function getCountriesAvailableForShipping(): array{
        return ShippingModel::where('active', 1)->groupBy('country_id')->pluck('country_id')->toArray();
    }

    /**
     * Devuelve las zonas de envío existentes a partir del ID de un país
     * @param int $countryId
     * @return Collection
     */
    public function getShippingRelatedToCountry(int $countryId): Collection{
        return ShippingModel::where('active', 1)->where('country_id', $countryId)->get();
    }
}

<?php

namespace Bittacora\Shipping\Helpers;

use Bittacora\LivewireCountryStateSelector\Models\Country;
use Bittacora\LivewireCountryStateSelector\Models\State;
use Bittacora\Shipping\Models\ShippingModel;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ShippingHelper
{
    public static function createCountry(string $name): Country{
        $country = Country::create(['name' => $name]);
        return $country;
    }

    public static function createStates(Country $country, int $count = 1): Collection{
        for($i = 0; $i < $count; $i++){
            $state = new State();
            $state->setCountryId($country->getAttribute('id'));
            $state->setName(fake()->text('15'));
            $state->save();
        }

        return State::where('country_id', $country->getAttribute('id'))->get();
    }

    public static function createShipping(array $shippingAttributes = [], bool $withStates= false){
        $country = self::createCountry('España');
        if($withStates){
            $states = self::createStates($country, 5);
            $country->states()->where('country_id', $country->getAttribute('id'));
        }

        if(!empty($shippingAttributes)){
            $shippingAttributes = array_merge($shippingAttributes, ['country_id' => $country->getAttribute('id')]);
        }
        $shipping = ShippingModel::factory()->create($shippingAttributes);


        return $shipping;
    }

    public static function provideShippingUpdatingData(ShippingModel $shipping){
        return [
            'name' => 'Nigeria',
            'country_id' => $shipping->country->id,
            'active' => $shipping->getAttribute('active'),
            'free_shipping' => $shipping->getAttribute('free_shipping'),
            'by_weight' => $shipping->getAttribute('by_weight')
        ];
    }

    public static function createSingleState(int $countryId){
        $state = new State();
        $state->setName(fake()->text('15'));
        $state->setCountryId($countryId);
        $state->save();

        return $state;
    }

    public static function provideShippingWeightData(){
        $minWeightUpdatingData = [
            0 => 0,
            1 => 5.01,
            2 => 10.01
        ];

        $maxWeightUpdatingData = [
            0 => 5,
            1 => 10,
            2 => 15
        ];

        $priceIncludingVatUpdatingData = [
            0 => 19.99,
            1 => 29.99,
            2 => 39.99
        ];

        $weightUpdatingData = [
            'min_weight' => $minWeightUpdatingData, 'max_weight' => $maxWeightUpdatingData, 'price_including_vat' => $priceIncludingVatUpdatingData
        ];

        return $weightUpdatingData;
    }

    public static function createShippingWeightData(ShippingModel $shipping, float|int $vat = 21): array{
        $weightData = self::provideShippingWeightData();
        for($i = 0; $i < count($weightData['min_weight']); $i++){
            DB::table('shipping_weight')->insert([
                'shipping_id' => $shipping->getAttribute('id'),
                'min_weight' => $weightData['min_weight'][$i],
                'max_weight' => $weightData['max_weight'][$i],
                'price_including_vat' => $weightData['price_including_vat'][$i],
                'price_excluding_vat' => round($weightData['price_including_vat'][$i]/(1+($vat/100)),2),
                'price_vat' => round($weightData['price_including_vat'][$i] - ($weightData['price_including_vat'][$i]/(1+($vat/100))),2)
            ]);
        }

        $data = DB::table('shipping_weight')->where('shipping_id', $shipping->getAttribute('id'))->get()->toArray();

        return $data;
    }

    public static function provideShippingPriceRangeData(): array
    {
        $priceRangeUpdatingData = [
            'min_price' => [
                0 => 0,
                1 => 50.01,
                2 => 100.01
            ],
            'max_price' => [
                0 => 50,
                1 => 100,
                2 => 150
            ],
            'range_price_including_vat' => [
                0 => 19.99,
                1 => 29.99,
                2 => 39.99
            ]
        ];

        return $priceRangeUpdatingData;
    }

    public static function createShippingPriceRangeData(Model|Collection|ShippingModel $shipping, int|float $vat = 21): array
    {
        $priceRangeData = self::provideShippingPriceRangeData();
        for($i = 0; $i < count($priceRangeData['min_price']); $i++){
            DB::table('shipping_price_range')->insert([
                'shipping_id' => $shipping->getAttribute('id'),
                'min_price' => $priceRangeData['min_price'][$i],
                'max_price' => $priceRangeData['max_price'][$i],
                'price_including_vat' => $priceRangeData['range_price_including_vat'][$i],
                'price_excluding_vat' => round($priceRangeData['range_price_including_vat'][$i]/(1+($vat/100)),2),
                'price_vat' => round($priceRangeData['range_price_including_vat'][$i] - ($priceRangeData['range_price_including_vat'][$i]/(1+($vat/100))),2)
            ]);
        }

        $data = DB::table('shipping_price_range')->where('shipping_id', $shipping->getAttribute('id'))->get()->toArray();

        return $data;
    }
}

# Shipping

<h3>Resumen</h2>
Paquete para gestionar gastos de envío de un comercio
electrónico. Este paquete permite gestionar gastos de envío según:

- Peso
- Rangos de precios

Queda pendiente eliminar una dependencia que, a día de hoy
(01/12/2023) utiliza un paquete de gestión de impuestos
para determinar el iva aplicable a una zona de envío.

Toda zona de envío debe ir asociada a un país y a unas regiones
de ese país determinadas.

Para instalar el paquete: 

<code>composer require bittacora/shipping-weight-range-price</code>

Tras instalar el paquete, ejecutar el siguiente comando:

<code>php artisan shipping:install</code>

Este paquete trabaja con otro asociado como dependencia:

<code>
"bittacora/livewire-countries-states-select": "0.x"
</code>

<h3>Funcionalidades</h3>

Este paquete cuenta con un Facade donde quedan registrados
métodos de acceso a las funcionalidades del paquete.

<h5>Obtenemos gastos de envío por rango de precios asociados a una zona de envío</h5>
<code>
    public function <span style="color:red">getShippingByPriceRange</span>
    (ShippingModel <span style="color:lightgreen">$shipping</span>, float <span style="color:lightgreen">$subtotal</span>, null|int <span style="color:lightgreen">$stateSelectedForShipping</span>)
</code>

<h5>
    Devuelve los gastos de envío por peso asociados a una zona de envío
</h5>
<code>
    public function <span style="color:red">getShippingByWeight</span>(ShippingModel <span style="color:lightgreen">$shipping</span>, float <span style="color:lightgreen">$weight</span>, null|int <span style="color:lightgreen">$stateSelectedForShipping</span>)
</code>

<h5>
    Devuelve la zona de envío a la que pertenece un estado/provincia
<h5>
<code>
    public function <span style="color:red">getShippingByState</span>(int <span style="color:lightgreen">$stateId</span>)
</code>

<h5>
    Devuelve todos los países que están asociados a alguna zona de envío
</h5>
<code>
    public function <span style="color:red">getCountriesAvailableForShipping</span>()
</code>

<h5>
    Devuelve todos los estados que estén asociados a alguna zona de envío
</h5>
<code>
    public function <span style="color:red">getSelectedAreasForShipping</span>
        (Collection|ShippingModel <span style="color:lightgreen">$model</span>)
</code>

<h5>
    Obtenemos zonas de envío asociadas a un país
</h5>

<code>
    public function <span style="color:red">getShippingRelatedToCountry</span>(int <span style="color:lightgreen">$countryId</span>)
</code>




<h3>Tests</h3>
El paquete contiene, a día de hoy, 22 tests que analizan su comportamiento,
aunque seguro que hay casos de uso no contemplados y algunas
inconsistencias que hay que pulir.

Para poder añadir los tests al proyecto en el cual se esté utilizando
se agrega al <code>phpunit.xml</code> del proyecto de Laravel lo siguiente
entre las etiquetas "testsuite":

<code>
    < testsuite name="Shipping">
         < directory>packages/shipping/tests< /directory>
    < /testsuite>
</code>

Obsérvese la separación en los símbolos de las etiquets (no me dejaba
ponerlas correctamente).

Para ejecutar los tests después de agregar esas etiquetas:

<code>
    php artisan test --testsuite=Shipping
</code>

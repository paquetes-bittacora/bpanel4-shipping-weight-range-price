<?php

use Bittacora\Shipping\Http\Controllers\ShippingController;
use Illuminate\Support\Facades\Route;

Route::prefix('bpanel')->middleware(['web', 'auth', 'admin-menu'])->name('shipping.')->group(function () {
    Route::get('shipping', [ShippingController::class, 'index'])->name('index');
    Route::get('shipping/create', [ShippingController::class, 'create'])->name('create');
    Route::post('shipping/store', [ShippingController::class, 'store'])->name('store');
    Route::get('shipping/{model}/edit', [ShippingController::class, 'edit'])->name('edit');
    Route::put('shipping/{model}/update', [ShippingController::class, 'update'])->name('update');
    Route::delete('shipping/{model}', [ShippingController::class, 'destroy'])->name('destroy');
});

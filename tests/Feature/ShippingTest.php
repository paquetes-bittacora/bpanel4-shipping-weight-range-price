<?php

namespace Bittacora\Shipping\Tests\Feature;

use Bittacora\LivewireCountryStateSelector\Models\Country;
use Bittacora\Shipping\Helpers\ShippingHelper;
use Bittacora\Shipping\Infrastructure\ShippingPriceRangeRepository;
use Bittacora\Shipping\Infrastructure\ShippingWeightRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ShippingTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     */
    public function test_cliente_solo_ve_paises_que_se_encuentran_en_zonas_de_envio(): void
    {
        $shipping = ShippingHelper::createShipping(['name' => 'Península']);

        $countryToShow = Country::where('id', $shipping->getAttribute('country_id'))->first();

        $view = $this->view('shipping::tests.checkout', ['countryName' => $shipping->country->getAttribute('name'), 'stateNames' => []]);

        $view->assertSee('España');
        $view->assertDontSee('Portugal');
    }

    public function test_cliente_solo_ve_estados_que_se_encuentran_en_zonas_de_envio(){
        $shipping = ShippingHelper::createShipping(['name' => 'Peninsula'], true);

        $view = $this->view('shipping::tests.checkout', [
            'countryName' => $shipping->country->getAttribute('name'),
            'stateNames' => $shipping->country->states()->pluck('name')->toArray()
        ]);

        foreach($shipping->country->states as $state){
            $view->assertSee($state->getAttribute('name'));
        }

        $view->assertDontSee('Estado imposible de encontrar');
    }

    public function test_se_reciben_gastos_de_envio_por_peso_correctamente_con_el_iva_por_defecto_aplicado(){
        $shipping = ShippingHelper::createShipping(['name' => 'Peninsula'], true);
        ShippingHelper::createShippingWeightData($shipping);

        $shippingRepository = new ShippingWeightRepository();

        $shipping = $shippingRepository->getShipping($shipping, 3);

        // Assert -> Se esperan 19.99 según los datos creados
        $this->assertEquals(19.99, $shipping);
    }

    public function test_se_reciben_gastos_de_envio_por_peso_correctamente_con_el_iva_de_region_aplicado(){
        $shipping = ShippingHelper::createShipping(['name' => 'Peninsula'], true);
        $shippingWeight = ShippingHelper::createShippingWeightData($shipping, 10);

        $shippingRepository = new ShippingWeightRepository(10);
        $shipping = $shippingRepository->getShipping($shipping, 3);

        foreach($shippingWeight as $item){
            if($item->min_weight <= 3 and $item->max_weight >= 3){
                $this->assertEquals($shipping, $item->price_including_vat);
                $this->assertEquals(round($shipping/(1+(10/100)),2), $item->price_excluding_vat);
                $this->assertEquals(round($shipping-($shipping/(1+(10/100))), 2), $item->price_vat);
            }
        }
    }

    public function test_se_reciben_gastos_de_envio_por_rango_de_precio_correctamente_con_el_iva_por_defecto_aplicado(){
        $shipping = ShippingHelper::createShipping(['name' => 'Peninsula'], true);
        ShippingHelper::createShippingPriceRangeData($shipping);

        $shippingRepository = new ShippingPriceRangeRepository();

        $shipping = $shippingRepository->getShipping($shipping, 3);

        // Assert -> Se esperan 19.99 según los datos creados
        $this->assertEquals(19.99, $shipping);
    }

    public function test_se_reciben_gastos_de_envio_por_rango_de_precios_correctamente_con_el_iva_de_region_aplicado(){
        $shipping = ShippingHelper::createShipping(['name' => 'Peninsula'], true);
        $shippingPriceRange = ShippingHelper::createShippingPriceRangeData($shipping, 10);

        $shippingRepository = new ShippingPriceRangeRepository(10);
        $shipping = $shippingRepository->getShipping($shipping, 49);

        foreach($shippingPriceRange as $item){
            if($item->min_price <= 3 and $item->max_price >= 3){
                $this->assertEquals($shipping, $item->price_including_vat);
                $this->assertEquals(round($shipping/(1+(10/100)),2), $item->price_excluding_vat);
                $this->assertEquals(round($shipping-($shipping/(1+(10/100))), 2), $item->price_vat);
            }
        }
    }

}

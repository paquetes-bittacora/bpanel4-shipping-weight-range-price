<?php

namespace Bittacora\Shipping\Tests\Feature;

use Bittacora\Bpanel4Users\Models\User;
use Bittacora\Shipping\Helpers\ShippingHelper;
use Bittacora\Shipping\Models\ShippingModel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class BpanelShippingTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     */
    public function test_usuario_con_permisos_puede_acceder_al_listado_de_zonas_de_envio(): void
    {
        $this->usuario_con_permiso_especifico('shipping.index');
        $response = $this->json('GET', '/bpanel/shipping', []);

        $response->assertStatus(200);
    }

    public function test_usuario_sin_permisos_no_puede_acceder_al_listado_de_zonas_de_envio(): void
    {
        $this->usuario_sin_permisos();

        $response = $this->get('/bpanel/shipping');

        $response->assertStatus(403);
    }

    public function test_usuario_con_permisos_puede_acceder_a_la_creacion_de_zonas_de_envio(): void
    {
        $this->usuario_con_permiso_especifico('shipping.create');

        $response = $this->get('/bpanel/shipping/create');

        $response->assertStatus(200);
    }

    public function test_usuario_sin_permisos_no_puede_acceder_a_la_creacion_de_zonas_de_envio(): void
    {
        $this->usuario_sin_permisos();

        $response = $this->get('/bpanel/shipping/create');

        $response->assertStatus(403);
    }

    public function test_zona_de_envio_puede_ser_creada(): void
    {
        $this->usuario_con_permiso_especifico('shipping.store');
        $shippingData = ShippingModel::factory()->make()->toArray();

        $response = $this->json('POST', '/bpanel/shipping/store', $shippingData);

        $this->assertDatabaseHas('shipping', $shippingData);
        $response->assertStatus(302);
        $shippingModel = ShippingModel::where('name', $shippingData['name'])->first();

        $response->assertRedirect(route('shipping.edit', ['model' => $shippingModel]));
    }

    public function test_zona_de_envio_debe_tener_nombre_en_creacion(): void
    {
        $this->usuario_con_permiso_especifico('shipping.store');
        $shippingData = ShippingModel::factory()->make(['name' => ''])->toArray();

        $response = $this->json('POST', '/bpanel/shipping/store', $shippingData);
        $response->assertStatus(422);
        $this->assertArrayHasKey('name', $response->decodeResponseJson()['errors']);
    }

    public function test_zona_de_envio_debe_tener_pais_seleccionado_en_creacion(): void
    {
        $this->usuario_con_permiso_especifico('shipping.store');
        $shippingData = ShippingModel::factory()->make(['country_id' => ''])->toArray();

        $response = $this->json('POST', '/bpanel/shipping/store', $shippingData);
        $response->assertStatus(422);
        $this->assertArrayHasKey('country_id', $response->decodeResponseJson()['errors']);
    }

    public function test_usuario_puede_acceder_a_edicion_de_zona_de_envio(){
        $this->withoutExceptionHandling();
        $this->usuario_con_permiso_especifico('shipping.edit');

        $country = ShippingHelper::createCountry('España');
        $model = ShippingModel::factory()->create(['country_id' => $country->getAttribute('id')]);
        $response = $this->get(route('shipping.edit', ['model' => $model]));

        $response->assertStatus(200);
        $response->assertSee([
            $model->getAttribute('name'), $model->getCountryAttribute()->name,
            $model->getAttribute('active'), $model->getAttribute('by_weight'),
            $model->getAttribute('free_shipping')
        ]);


    }

    public function test_todos_los_estados_de_un_pais_aparecen_en_la_edicion_de_una_zona_de_envio()
    {
        $this->usuario_con_permiso_especifico('shipping.edit');
        $this->withoutExceptionHandling();

        $country = ShippingHelper::createCountry('España');
        $states = ShippingHelper::createStates($country, 5);
        $model = ShippingModel::factory()->create(['country_id' => $country->getAttribute('id')]);

        $response = $this->get(route('shipping.edit', ['model' => $model]));

        foreach($states as $state){
            $response->assertSee($state->getAttribute('name'));
        }
    }

    public function test_se_pueden_actualizar_los_datos_de_una_zona_de_envio(){
        $this->withoutExceptionHandling();
        $this->usuario_con_permiso_especifico('shipping.update');
        $shipping = ShippingHelper::createShipping(['name' => 'España'],true);
        $response = $this->json('PUT', "/bpanel/shipping/{$shipping->id}/update", ShippingHelper::provideShippingUpdatingData($shipping));
        $this->assertDatabaseHas('shipping', ShippingHelper::provideShippingUpdatingData($shipping));
        $response->assertStatus(302);
    }

    public function test_faltan_datos_basicos_al_actualizar_una_zona_de_envio(){
        $this->usuario_con_permiso_especifico('shipping.update');
        $shipping = ShippingHelper::createShipping(['name' => 'España'],true);
        $response = $this->json('PUT', "/bpanel/shipping/{$shipping->id}/update", []);
        $response->assertStatus(422);
        $decodedJsonResponse = $response->decodeResponseJson();
        $this->assertArrayHasKey('name', $decodedJsonResponse['errors']);
        $this->assertArrayHasKey('country_id', $decodedJsonResponse['errors']);
        $this->assertArrayHasKey('active', $decodedJsonResponse['errors']);
        $this->assertArrayHasKey('by_weight', $decodedJsonResponse['errors']);
        $this->assertArrayHasKey('free_shipping', $decodedJsonResponse['errors']);
    }

    public function test_se_pueden_actualizar_los_estados_de_una_zona_de_envio(){
        $this->usuario_con_permiso_especifico('shipping.update');
        $shipping = ShippingHelper::createShipping(['name' => 'España'],true);
        $shippingUpdatingData = ShippingHelper::provideShippingUpdatingData($shipping);
        $stateUpdatingData = ShippingHelper::createSingleState($shipping->country->getAttribute('id'));
        $shippingUpdatingData['state_id'] = [$stateUpdatingData->getAttribute('id')];
        $response = $this->json('PUT', "/bpanel/shipping/{$shipping->id}/update",
            array_merge($shippingUpdatingData, $shippingUpdatingData));

        $this->assertDatabaseHas('shipping_area', [
            'shipping_id' => $shipping->getAttribute('id'),
            'state_id' => $stateUpdatingData->getAttribute('id')]);
        $response->assertStatus(302);
    }

    public function test_se_pueden_actualizar_los_pesos_de_una_zona_de_envio(){
        $this->usuario_con_permiso_especifico('shipping.update');
        $shipping = ShippingHelper::createShipping(['name' => 'España'],true);
        $shippingUpdatingData = ShippingHelper::provideShippingUpdatingData($shipping);

        $weightUpdatingData = ShippingHelper::provideShippingWeightData();
        $this->json('PUT', "/bpanel/shipping/{$shipping->id}/update",
            array_merge($shippingUpdatingData, $weightUpdatingData));

        for($i=0; $i < count($weightUpdatingData['min_weight']); $i++){
            $this->assertDatabaseHas('shipping_weight', [
                'shipping_id' => $shipping->getAttribute('id'),
                'min_weight' => $weightUpdatingData['min_weight'][$i],
                'max_weight' => $weightUpdatingData['max_weight'][$i],
                'price_including_vat' => $weightUpdatingData['price_including_vat'][$i]
            ]);
        }
    }

    public function test_se_pueden_actualizar_los_rangos_de_precio_de_una_zona_de_envio(){
        $this->usuario_con_permiso_especifico('shipping.update');
        $shipping = ShippingHelper::createShipping(['name' => 'España'],true);
        $shippingUpdatingData = ShippingHelper::provideShippingUpdatingData($shipping);

        $priceRangeUpdatingData = ShippingHelper::provideShippingPriceRangeData();

        $this->json('PUT', "/bpanel/shipping/{$shipping->id}/update",
            array_merge($shippingUpdatingData, $priceRangeUpdatingData));

        for($i=0; $i < count($priceRangeUpdatingData['min_price']); $i++){
            $this->assertDatabaseHas('shipping_price_range', [
                'shipping_id' => $shipping->getAttribute('id'),
                'min_price' => $priceRangeUpdatingData['min_price'][$i],
                'max_price' => $priceRangeUpdatingData['max_price'][$i],
                'price_including_vat' => $priceRangeUpdatingData['range_price_including_vat'][$i]
            ]);
        }
    }

    public function test_se_puede_eliminar_una_zona_de_envio(){
        $this->usuario_con_permiso_especifico('shipping.destroy');
        $shipping = ShippingHelper::createShipping(['name' => 'España'],true);
        $response = $this->json('DELETE', "/bpanel/shipping/{$shipping->id}", $shipping->toArray());

        $response->assertStatus(302);
        $this->assertDatabaseMissing('shipping', $shipping->toArray());
    }

    public function test_se_puede_eliminar_una_zona_de_envio_con_todas_sus_tarifas(){
        $this->usuario_con_permiso_especifico('shipping.destroy');
        $shipping = ShippingHelper::createShipping(['name' => 'España'],true);
        ShippingHelper::createShippingWeightData($shipping);
        ShippingHelper::createShippingPriceRangeData($shipping);
        $response = $this->json('DELETE', "/bpanel/shipping/{$shipping->id}", $shipping->toArray());
        $this->assertDatabaseMissing('shipping_weight', ['shipping_id' => $shipping->getAttribute('id')]);
        $this->assertDatabaseMissing('shipping_price_range', ['shipping_id' => $shipping->getAttribute('id')]);
    }

    private function usuario_con_permiso_especifico($permission){
        $user = User::factory()->create();
        $roleAdmin = Role::findOrCreate('admin');
        $permission = Permission::create(['name' => $permission]);
        $roleAdmin->givePermissionTo($permission);
        $user->assignRole($roleAdmin);
        auth()->login($user);
    }

    private function usuario_sin_permisos(){
        $user = User::factory()->create();
        auth()->login($user);
    }
}

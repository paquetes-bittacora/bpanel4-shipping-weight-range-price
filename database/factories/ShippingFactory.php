<?php

namespace Bittacora\Shipping\Database\Factories;

use Bittacora\LivewireCountryStateSelector\Models\Country;
use Bittacora\Shipping\Models\ShippingModel;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class ShippingFactory extends Factory
{
    protected $model = ShippingModel::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->text('15'),
            'country_id' => fake()->numberBetween(1, 29),
            'active' => fake()->boolean ? 1 : 0,
            'free_shipping' => fake()->boolean ? 1 : 0,
            'by_weight' => fake()->boolean ? 1 : 0

        ];
    }
}

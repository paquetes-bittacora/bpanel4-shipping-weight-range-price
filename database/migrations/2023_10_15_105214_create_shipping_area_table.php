<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_area', function (Blueprint $table) {
            $table->unsignedBigInteger('shipping_id');
            $table->unsignedBigInteger('state_id');

            $table->unique(['shipping_id', 'state_id']);
            $table->foreign('shipping_id')->references('id')->on('shipping')->cascadeOnDelete();
            $table->foreign('state_id')->references('id')->on('states')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('shipping_area');
        Schema::enableForeignKeyConstraints();
    }
};

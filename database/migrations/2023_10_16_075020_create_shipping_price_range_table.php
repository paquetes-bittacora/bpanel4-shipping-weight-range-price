<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_price_range', function (Blueprint $table) {
            $table->unsignedBigInteger('shipping_id');
            $table->unsignedDouble('min_price', 8, 2)->nullable();
            $table->unsignedDouble('max_price', 8, 2)->nullable();
            $table->unsignedDouble('price_including_vat', 8, 2)->nullable();
            $table->unsignedDouble('price_excluding_vat', 8, 2)->nullable();
            $table->unsignedDouble('price_vat', 8, 2)->nullable();

            $table->foreign('shipping_id')->references('id')->on('shipping')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('shipping_price_range');
        Schema::enableForeignKeyConstraints();
    }
};

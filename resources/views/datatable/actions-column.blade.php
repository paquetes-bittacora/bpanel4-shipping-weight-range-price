<div class="text-center">
    @livewire('utils::datatable-action-buttons', ['actions' => ["edit", "delete"], 'scope' => 'shipping', 'model' => $row, 'permission' => ['edit', 'delete'], 'id' => $row->id, 'message' => 'la zona de envío?'], key('shipping-buttons-'.$row->id))
</div>

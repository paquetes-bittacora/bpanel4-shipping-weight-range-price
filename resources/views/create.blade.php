@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Crear zona de envío')

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0 d-flex justify-content-between">
            <h4 class="text-120 mb-0">
                <span class="text-90">Crear zona de envío</span>
            </h4>
        </div>

        <form class="mt-lg-3" autocomplete="off" method="post" action="{{route('shipping.store')}}" enctype="multipart/form-data">
            @csrf
            @livewire('form::input-text', ['name' => 'name', 'labelText' => 'Nombre', 'required'=>true, 'value' => old('name')])
            @livewire('form::select', ['name' => 'country_id', 'labelText' => 'País', 'allValues' => $countries, 'required' => true])
            @livewire('form::input-checkbox', ['name' => 'active', 'value' => 1, 'labelText' => 'Activo', 'bpanelForm' => true, 'checked' => true])
            @livewire('form::input-checkbox', ['name' => 'free_shipping', 'value' => 1, 'labelText' => 'Envío gratuito', 'bpanelForm' => true, 'checked' => false])
            @livewire('form::input-checkbox', ['name' => 'by_weight', 'value' => 1, 'labelText' => 'Envío por peso', 'bpanelForm' => true, 'checked' => false])


            <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
                @livewire('form::save-button',['theme'=>'save'])
                @livewire('form::save-button',['theme'=>'reset'])
            </div>
        </form>
    </div>
@endsection

@push('scripts')
    <script src="{{ Vite::asset('node_modules/jquery/dist/jquery.js') }}"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            $("#country_id").select2();
        });
    </script>
@endpush

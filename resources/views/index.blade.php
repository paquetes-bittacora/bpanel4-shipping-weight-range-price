@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Zonas de envío')

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">Listado de zonas de envíos</span>
            </h4>
        </div>
        <div class="card-body">
            @livewire('shipping::shipping-datatable')
        </div>
    </div>
@endsection

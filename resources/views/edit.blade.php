@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Editar zona de envío')

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0 d-flex justify-content-between">
            <h4 class="text-120 mb-0">
                <span class="text-90">Editar zona de envío</span>
            </h4>
        </div>

        <form class="mt-lg-3" autocomplete="off" method="post" action="{{route('shipping.update', ['model' => $model])}}" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            @livewire('form::input-text', ['name' => 'name', 'labelText' => 'Nombre', 'required'=>true, 'value' => $model->name])
            <div class="form-row form-group control-label">
                <label for="" class="col-sm-3 text-right">
                    País
                </label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" disabled value="{{ $model->getCountryAttribute()->name }}">
                </div>
            </div>

            @livewire('form::input-checkbox', [
                'name' => 'active',
                'value' => 1,
                'labelText' => 'Activo',
                'bpanelForm' => true,
                'checked' => $model->active == 1,
            ])

            @livewire('form::input-checkbox', [
                'name' => 'free_shipping',
                'value' => 1,
                'labelText' => 'Envío gratuito',
                'bpanelForm' => true,
                'checked' => $model->free_shipping == 1,
            ])

            @livewire('form::input-checkbox', [
                'name' => 'by_weight',
                'value' => 1,
                'labelText' => 'Envío por peso',
                'bpanelForm' => true,
                'checked' => $model->by_weight == 1,
            ])

            <div class="card b-card p-3">
                <div class="card-header bgc-primary-d1 text-white border-0">
                    <h4 class="text-120">
                        <span class="text-90">Selección de provincias / estados</span>
                    </h4>
                </div>

                <div class="bootstrap-duallistbox-container row moveonselect moveondoubleclick mb-4 pt-2 pb-2">
                    <div class="box1 col-12">
                        <select multiple="multiple" id="state_id" class="form-control" name="state_id[]" style="height: 450px;">
                            @forelse($states as $key => $value)
                                <option value="{{$key}}" @if(in_array($key, $selectedAreas)) selected @endif>{{ $value }}</option>
                                {{--                            <option value="{{$key}}">{{ $value }}</option>--}}
                            @empty
                                <option value="">-- No hay datos --</option>
                            @endforelse
                        </select>

                        <small>
                            Es obligatorio seleccionar las provincias o estados para los que estarán disponibles los envíos. Si no selecciona
                            una provincia los clientes no podrán indicar envíos a esas ubicaciones.
                        </small>
                    </div>
                </div>
            </div>

            <!-- RANGOS DE PESO -->
            <div class="card b-card mt-3 p-3">
                <div class="card-header bgc-primary-d1 text-white border-0">
                    <h4 class="text-120">
                        <span class="text-90">Precios por tramos de peso (debe marcar esta zona con envíos por peso para que esta configuración surta efecto)</span>
                    </h4>
                </div>

                <div class="card-body cuerpo-tramos-peso">
                    @if(!empty($selectedWeight))
                        @foreach($selectedWeight as $key => $value)
                            <div class="form-group form-row fila-tramos-peso">
                                <div class="col-sm-4">
                                    <span class="d-block">Peso mínimo (en kilos, separe decimales con puntos)</span>
                                    <input type="text" name="min_weight[{{$key+1}}]" class="form-control" value="{{$value->min_weight}}">
                                </div>
                                <div class="col-sm-4">
                                    <span class="d-block">Peso máximo (en kilos, separe decimales con puntos)</span>
                                    <input type="text" name="max_weight[{{$key+1}}]" class="form-control" value="{{$value->max_weight}}">
                                </div>
                                <div class="col-sm-4">
                                    <span class="d-block">Precio (Iva incluido, separe decimales con puntos)</span>
                                    <div class="d-flex">
                                        <input type="text" name="price_including_vat[{{$key+1}}]" class="form-control" value="{{$value->price_including_vat}}">
                                        <button class="borrar-tramos-peso btn btn-danger"><i class="fas fa-trash"></i></button>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif

                </div>
                <div class="form-group">
                    <button id="botonTramosPeso" class="btn btn-info"><i class="fas fa-plus"></i>Añadir nuevo tramo</button>
                </div>
            </div>

            <!-- RANGOS DE PRECIO -->
            <div class="card b-card mt-3 p-3">
                <div class="card-header bgc-primary-d1 text-white border-0">
                    <h4 class="text-120">
                        <span class="text-90">Importes por tramos de precios (debe DESMARCAR esta zona con envíos por peso para que esta configuración surta efecto)</span>
                    </h4>
                </div>

                <div class="card-body cuerpo-tramos-precio">
                    @if(!empty($selectedPriceRange))
                        @foreach($selectedPriceRange as $key => $value)
                            <div class="form-group form-row fila-tramos-precio">
                                <div class="col-sm-4">
                                    <span class="d-block">Precio mínimo (separe decimales con puntos)</span>
                                    <input type="text" name="min_price[{{$key+1}}]" class="form-control" value="{{$value->min_price}}">
                                </div>
                                <div class="col-sm-4">
                                    <span class="d-block">Precio máximo (separe decimales con puntos)</span>
                                    <input type="text" name="max_price[{{$key+1}}]" class="form-control" value="{{$value->max_price}}">
                                </div>
                                <div class="col-sm-4">
                                    <span class="d-block">Precio (Iva incluido, separe decimales con puntos)</span>
                                    <div class="d-flex">
                                        <input type="text" name="range_price_including_vat[{{$key+1}}]" class="form-control" value="{{$value->price_including_vat}}">
                                        <button class="borrar-tramos-peso btn btn-danger"><i class="fas fa-trash"></i></button>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
                <div class="form-group">
                    <button id="botonTramosPrecio" class="btn btn-info"><i class="fas fa-plus"></i>Añadir nuevo tramo</button>
                </div>
            </div>

            <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
                @livewire('form::save-button',['theme'=>'save'])
                @livewire('form::save-button',['theme'=>'reset'])
            </div>
            @livewire('form::input-hidden', ['name' => 'id', 'value'=> $model->id])
            @livewire('form::input-hidden', ['name' => 'country_id', 'value'=> $model->country_id])

        </form>
    </div>
@endsection

@push('scripts')
    <script src="{{ Vite::asset('node_modules/jquery/dist/jquery.js') }}"></script>
    <script>
        // DUAL LIST BOX DE PROVINCIAS
        document.addEventListener('DOMContentLoaded' ,function(){
            $("#state_id").bootstrapDualListbox({
                filterTextClear: 'Mostrar todos',
                filterPlaceHolder: 'Filtrar',
                moveSelectedLabel: 'Mover seleccionados',
                moveAllLabel: 'Mover todos',
                removeSelectedLabel: 'Quitar seleccionados',
                removeAllLabel: 'Quitar todos',
                infoText: 'Mostrando todos ({0})',
                infoTextFiltered: '<span class="label label-warning">Filtrando</span> {0} de {1}',
                infoTextEmpty: "Lista vacía"
            });
            $('.moveall i').removeClass().addClass('fa fa-arrow-right');
            $('.move i').removeClass().addClass('fa fa-arrow-right');
            $('.removeall i').removeClass().addClass('fa fa-arrow-left');
            $('.remove i').removeClass().addClass('fa fa-arrow-left');
        });

        // MÓDULO DE TRAMOS POR PESO
        var filasTramosPeso = $(".fila-tramos-peso").length;

        $("#botonTramosPeso").on("click", function(e){
            e.preventDefault();
            filasTramosPeso = parseInt(filasTramosPeso)+1;

            var nuevaFila = '<div class="form-group form-row fila-tramos-peso">'+
                                '<div class="col-sm-4">'+
                                    '<span class="d-block">Peso mínimo (en kilos, separe decimales con puntos)</span>'+
                                    '<input type="text" name="min_weight['+filasTramosPeso+']" class="form-control">'+
                                '</div>'+
                                '<div class="col-sm-4">'+
                                    '<span class="d-block">Peso máximo (en kilos, separe decimales con puntos)</span>'+
                                    '<input type="text" name="max_weight['+filasTramosPeso+']" class="form-control">'+
                                '</div>'+
                                '<div class="col-sm-4">'+
                                    '<span class="d-block">Precio (Iva incluido, separe decimales con puntos)</span>'+
                                    '<div class="d-flex">'+
                                        '<input type="text" name="price_including_vat['+filasTramosPeso+']" class="form-control">'+
                                        '<button class="borrar-tramos-peso btn btn-danger"><i class="fas fa-trash"></i></button>'+
                                    '</div>'+
                                '</div></div>';

            $(".cuerpo-tramos-peso").append(nuevaFila);
        });

        $("body").on("click", ".borrar-tramos-peso", function(e){
            e.preventDefault();
            $(this).parents('.fila-tramos-peso').remove();
        });


        // @todo Esto habrá que moverlo a un script externo
        // MÓDULOS DE TRAMOS POR PESO

        var filasTramosPrecio = $(".fila-tramos-precio").length;

        $("#botonTramosPrecio").on("click", function(e){
            e.preventDefault();
            filasTramosPrecio = parseInt(filasTramosPrecio)+1;

            var nuevaFilaPrecio = '<div class="form-group form-row fila-tramos-peso">'+
                '<div class="col-sm-4">'+
                '<span class="d-block">Precio mínimo (separe decimales con puntos)</span>'+
                '<input type="text" name="min_price['+filasTramosPrecio+']" class="form-control">'+
                '</div>'+
                '<div class="col-sm-4">'+
                '<span class="d-block">Precio máximo (separe decimales con puntos)</span>'+
                '<input type="text" name="max_price['+filasTramosPrecio+']" class="form-control">'+
                '</div>'+
                '<div class="col-sm-4">'+
                '<span class="d-block">Precio (Iva incluido, separe decimales con puntos)</span>'+
                '<div class="d-flex">'+
                '<input type="text" name="range_price_including_vat['+filasTramosPrecio+']" class="form-control">'+
                '<button class="borrar-tramos-peso btn btn-danger"><i class="fas fa-trash"></i></button>'+
                '</div>'+
                '</div></div>';

            $(".cuerpo-tramos-precio").append(nuevaFilaPrecio);
        });
    </script>
@endpush
